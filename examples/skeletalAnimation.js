/// <reference path="../babylon.d.ts"/>

function run() {
  let canvas = document.getElementById('renderCanvas');
  let engine = new BABYLON.Engine(canvas, true);
  let scene = new BABYLON.Scene(engine);
  let camera = new BABYLON.FreeCamera('camera1', new BABYLON.Vector3(0, 5, -10), scene);

  camera.setTarget(BABYLON.Vector3.Zero());
  camera.attachControl(canvas, false);

  let light = new BABYLON.HemisphericLight('light1', new BABYLON.Vector3(0, 1, 0), scene);
  light.groundColor = new BABYLON.Color3(1, 0, 0);

  function renderFunction() {
    if (scene.activeCamera) {
      scene.render();
    }
  }
  engine.runRenderLoop(renderFunction);

  function loadDude() {
    engine.displayLoadingUI();
    engine.loadingUIText = 'loading';

    BABYLON.SceneLoader.ImportMesh("him", "./assets/meshes/Dude/", "dude.babylon", scene, function (newMeshes, particleSystems, skeletons) {
      let meshPart = 0;
      newMeshes.forEach((mesh) => {
        mesh.rotation.y = Math.PI;
        mesh.position = new BABYLON.Vector3(100, 0, -200);

        let cpyMesh = mesh.clone();
        // meshPart++;
        // cpyMesh.rotation.y = Math.PI;
        // cpyMesh.position = new BABYLON.Vector3(-100, 0, -200);
        // if(mesh.skeleton) {
        //   cpyMesh.skeleton = mesh.skeleton.clone();
        //   scene.beginAnimation(cpyMesh.skeleton, 0, 100, true, 1.0);
        // }
      })
      skeletons.forEach((skeleton)=>{
        scene.beginAnimation(skeleton, 0, 100, true, 1.0);
      })
      engine.hideLoadingUI();
    });
  }

  function loadElfWarrior() {

    engine.displayLoadingUI();
    engine.loadingUIText = 'loading';

    BABYLON.SceneLoader.ImportMesh("", "./assets/fbxExports/", "role_elf_warrior_run_001.babylon", scene, function (newMeshes, particleSystems, skeletons) {
      var dude = newMeshes[0];

      dude.rotation.y = Math.PI;
      dude.position = new BABYLON.Vector3(0, 0, -200);

      scene.beginAnimation(skeletons[0], 0, 100, true, 1.0);
      camera.setTarget(dude.position);
      engine.hideLoadingUI();
      loadDude();
    });

  }

  setTimeout(() => {
    camera.setTarget(new BABYLON.Vector3(0, 0, -200));
    loadDude();
  }, 100);

  window.addEventListener('resize', () => {
    engine.resize();
  });
}

(() => {
  return run;
})();