/// <reference path="../babylon.d.ts"/>

function run() {
  let canvas = document.getElementById('renderCanvas');
  let engine = new BABYLON.Engine(canvas, true);
  function createScene() {
    let scene = new BABYLON.Scene(engine);

    let camera = new BABYLON.FreeCamera('camera1', new BABYLON.Vector3(0, 5, -10), scene);

    camera.setTarget(BABYLON.Vector3.Zero());
    camera.attachControl(canvas, false);
    let light = new BABYLON.HemisphericLight('light1', new BABYLON.Vector3(0, 1, 0), scene);
    light.groundColor = new BABYLON.Color3(1, 0, 0);

    // create elements
    let box = BABYLON.Mesh.CreateBox('box', 6, scene);
    let sphere = BABYLON.Mesh.CreateSphere('sphere', 10, 10, scene);
    let plane = BABYLON.Mesh.CreatePlane('plane', 10, scene);
    let disc = BABYLON.Mesh.CreateDisc('disc', 5, 30, scene);
    let cylinder = BABYLON.Mesh.CreateCylinder('cylinder', 3, 3, 3, 6, 1, scene);
    let torus = BABYLON.Mesh.CreateTorus("torus", 5, 1, 10, scene);
    let knot = BABYLON.Mesh.CreateTorusKnot("knot", 2, 0.5, 128, 64, 2, 3, scene);

    return scene;
  }

  let scene = createScene();
  engine.runRenderLoop(() => {
    scene.render();
  });
  window.addEventListener('resize', () => {
    engine.resize();
  });
}

(() => {
  return run;
})();

