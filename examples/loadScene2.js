/// <reference path="../babylon.d.ts"/>

function run() {
  let canvas = document.getElementById('renderCanvas');
  let engine = new BABYLON.Engine(canvas, true);
  let scene = new BABYLON.Scene(engine);
  function renderFunction() {
    if( scene.activeCamera) {
      scene.render();
    }
  }
  engine.runRenderLoop(renderFunction);
  engine.displayLoadingUI();
  engine.loadingUIText = 'loading';
  BABYLON.SceneLoader.Append('./assets/meshes/', 'Rabbit.babylon', scene, (scene) => {
    engine.hideLoadingUI();
    if(scene.activeCamera) {
      scene.activeCamera.attachControl(canvas);
    }
  }, (progress) => {
    console.log(`load file to ${progress}.`);
  }, (scene) => {
    console.error('load file error');
    engine.hideLoadingUI();
  });

  window.addEventListener('resize', () => {
    engine.resize();
  });
}

(() => {
  return run;
})();