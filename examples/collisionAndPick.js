/// <reference path="../babylon.d.ts"/>

function run() {
  let canvas = document.getElementById('renderCanvas');
  let engine = new BABYLON.Engine(canvas, true);
  function createScene() {
    let scene = new BABYLON.Scene(engine);

    scene.collisionsEnabled = true;

    let camera = new BABYLON.FreeCamera('camera1', new BABYLON.Vector3(0, 5, -10), scene);
    camera.setTarget(BABYLON.Vector3.Zero());
    camera.attachControl(canvas, false);
    camera.checkCollisions = true;
    camera.ellipsoid = new BABYLON.Vector3(1, 1, 1);

    let box = null;
    let mtl = null;
    for (let index = -10; index < 10; ++index) {
      box = BABYLON.Mesh.CreateBox('crate', 2, scene);
      mtl = box.material = new BABYLON.StandardMaterial('mtl', scene);
      mtl.diffuseTexture = new BABYLON.Texture('./assets/textures/crate.png', scene);
      box.position.y = 1;
      box.position.x = -5 * index;
      box.checkCollisions = true;
    }

    let light = new BABYLON.HemisphericLight('light1', new BABYLON.Vector3(0, 1, 0), scene);
    light.diffuse = new BABYLON.Color3(0.8, 0.8, 0.8);
    light.groundColor = new BABYLON.Color3(0.2, 0.2, 0.2);

    let ground = BABYLON.Mesh.CreateGround('ground', 100, 100, 20, scene);
    mtl = ground.material = new BABYLON.StandardMaterial('mtl', scene);
    mtl.diffuseTexture = new BABYLON.Texture('./assets/textures/floor.png', scene);
    // mtl.diffuseColor = new BABYLON.Color3(255, 0, 0);
    ground.checkCollisions = true;
    return scene;
  }

  let scene = createScene();
  engine.runRenderLoop(() => {
    scene.render();
  });
  window.addEventListener('resize', () => {
    engine.resize();
  });

  window.addEventListener('click', () => {
    let pickResult = scene.pick(scene.pointerX, scene.pointerY);
    if (pickResult.hit) {
      pickResult.pickedMesh.material.diffuseColor = new BABYLON.Color3(255, 0, 0);
    }
  });
}

(() => {
  return run;
})();