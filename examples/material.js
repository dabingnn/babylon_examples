/// <reference path="../babylon.d.ts"/>

function run() {
  let canvas = document.getElementById('renderCanvas');
  let engine = new BABYLON.Engine(canvas, true);
  function createScene() {
    let scene = new BABYLON.Scene(engine);
    let light = new BABYLON.PointLight('Omni', new BABYLON.Vector3(0, 100, 100), scene);
    let camera = new BABYLON.ArcRotateCamera('camera', 0, 0.8, 100, BABYLON.Vector3.Zero(), scene);
    camera.attachControl(canvas, false);
    let sphere1 = BABYLON.Mesh.CreateSphere('sphere1', 10.0, 6.0, scene);
    let sphere2 = BABYLON.Mesh.CreateSphere('sphere1', 10.0, 7.0, scene);
    let sphere3 = BABYLON.Mesh.CreateSphere('sphere1', 10.0, 8.0, scene);
    sphere1.position.x = -40;
    sphere2.position.x = -30;
    let material1 = new BABYLON.StandardMaterial('mtl1', scene);
    sphere3.material = material1;
    let texturesDir = './assets/textures';
    material1.diffuseTexture = new BABYLON.Texture(`${texturesDir}/floor.png`, scene);
    return scene;
  }

  let scene = createScene();
  engine.runRenderLoop(() => {
    scene.render();
  });
  window.addEventListener('resize', () => {
    engine.resize();
  });
}

(() => {
  return run;
})();