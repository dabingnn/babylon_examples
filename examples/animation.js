/// <reference path="../babylon.d.ts"/>

function run() {
  let canvas = document.getElementById('renderCanvas');
  let engine = new BABYLON.Engine(canvas, true);
  function createScene() {
    let scene = new BABYLON.Scene(engine);

    let camera = new BABYLON.FreeCamera('camera1', new BABYLON.Vector3(0, 5, -10), scene);

    camera.setTarget(BABYLON.Vector3.Zero());
    camera.attachControl(canvas, false);
    let light = new BABYLON.HemisphericLight('light1', new BABYLON.Vector3(0, 1, 0), scene);
    light.groundColor = new BABYLON.Color3(1, 0, 0);

    // create elements
    let box = BABYLON.Mesh.CreateBox('box', 3, scene);
    box.position.x = 0;
    var animationBox = new BABYLON.Animation("myAnimation", "scaling.x",
      30, BABYLON.Animation.ANIMATIONTYPE_FLOAT, BABYLON.Animation.ANIMATIONLOOPMODE_CYCLE);
    let keys = [];
    keys.push({ frame: 0, value: 1 });
    keys.push({ frame: 20, value: 0.2 });
    keys.push({ frame: 100, value: 1 });
    animationBox.setKeys(keys);
    box.animations = [];
    box.animations.push(animationBox);
    scene.beginAnimation(box, 0, 100, true);
    return scene;
  }

  let scene = createScene();
  engine.runRenderLoop(() => {
    scene.render();
  });
  window.addEventListener('resize', () => {
    engine.resize();
  });
}

(() => {
  return run;
})();