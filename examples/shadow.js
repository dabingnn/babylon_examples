/// <reference path="../babylon.d.ts"/>

function run() {
  let canvas = document.getElementById('renderCanvas');
  let engine = new BABYLON.Engine(canvas, true);
  function createScene() {
    let scene = new BABYLON.Scene(engine);

    let camera = new BABYLON.FreeCamera('camera1', new BABYLON.Vector3(0, 20, -10), scene);
    camera.attachControl(canvas, false);

    let ground = BABYLON.Mesh.CreateGround('ground1', 30, 30, 2, scene);
    ground.receiveShadows = true;
    ground.position.y = 0;
    ground.position.z = 50;

    let sphere = BABYLON.Mesh.CreateSphere('sphere1', 16, 2, scene);
    sphere.position.y = 5;
    sphere.position.z = 50;

    let light = new BABYLON.DirectionalLight("*dir01", new BABYLON.Vector3(0, -0.6, 0.3), scene);
    let shadowGenerator05 = new BABYLON.ShadowGenerator(512, light);
    shadowGenerator05.getShadowMap().renderList.push(sphere);
    shadowGenerator05.usePoissonSampling = true;

    return scene;
  }

  let scene = createScene();
  engine.runRenderLoop(() => {
    scene.render();
  });
  window.addEventListener('resize', () => {
    engine.resize();
  });
}

(() => {
  return run;
})();