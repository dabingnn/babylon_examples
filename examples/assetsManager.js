/// <reference path="../babylon.d.ts"/>

function run() {
  let canvas = document.getElementById('renderCanvas');
  let engine = new BABYLON.Engine(canvas, true);
  let scene = new BABYLON.Scene(engine);
  let camera = new BABYLON.FreeCamera('camera1', new BABYLON.Vector3(0, 5, -10), scene);

  camera.setTarget(BABYLON.Vector3.Zero());
  camera.attachControl(canvas, false);
  let light = new BABYLON.HemisphericLight('light1', new BABYLON.Vector3(0, 1, 0), scene);
  light.groundColor = new BABYLON.Color3(1, 0, 0);
  let assetsMgr = new BABYLON.AssetsManager(scene);
  var meshTask = assetsMgr.addMeshTask("skull task", "", "./assets/meshes/", "skull.babylon");
  meshTask.onSuccess = function (task) {
    task.loadedMeshes[0].position = BABYLON.Vector3.Zero();
  };
  assetsMgr.onFinish = (tasks) => {
    engine.runRenderLoop(() => {
      scene.render();
    });
  };
  assetsMgr.load();
  window.addEventListener('resize', () => {
    engine.resize();
  });
}

(() => {
  return run;
})();