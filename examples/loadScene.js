/// <reference path="../babylon.d.ts"/>

function run() {
  let canvas = document.getElementById('renderCanvas');
  let engine = new BABYLON.Engine(canvas, true);

  BABYLON.SceneLoader.Load('./assets/meshes/', 'Rabbit.babylon', engine, (scene) => {
    scene.activeCamera.attachControl(canvas);
    engine.runRenderLoop(() => {
      scene.render();
    })
  }, function onProgress() {

  }, (scene) => {

  });
  window.addEventListener('resize', () => {
    engine.resize();
  });
}

(() => {
  return run;
})();