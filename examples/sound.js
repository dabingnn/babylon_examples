/// <reference path="../babylon.d.ts"/>

function run() {
  let canvas = document.getElementById('renderCanvas');
  let engine = new BABYLON.Engine(canvas, true);
  function createScene() {
    let scene = new BABYLON.Scene(engine);

    let camera = new BABYLON.FreeCamera('camera1', new BABYLON.Vector3(0, 5, -10), scene);

    camera.setTarget(BABYLON.Vector3.Zero());
    camera.attachControl(canvas, false);
    let light = new BABYLON.HemisphericLight('light1', new BABYLON.Vector3(0, 1, 0), scene);
    light.groundColor = new BABYLON.Color3(1, 0, 0);
    let sphere = BABYLON.Mesh.CreateSphere('sphere1', 16, 2, scene);
    sphere.position.y = 1;

    let ground = BABYLON.Mesh.CreateGround('ground1', 6, 6, 2, scene);
    ground.position.y = 0;
    return scene;
  }

  let scene = createScene();
  let music = new BABYLON.Sound('music', './assets/sounds/music_logo.mp3', scene, null, {
    loop: true, autoplay: true
  });
  let jump = new BABYLON.Sound('music', './assets/sounds/jump.wav', scene, null, {});
  let explode = new BABYLON.Sound('music', './assets/sounds/explosion.wav', scene, null, {});
  engine.runRenderLoop(() => {
    scene.render();
  });
  window.addEventListener('resize', () => {
    engine.resize();
  });
  window.addEventListener('keydown', (evt) => {
    if (evt.keyCode === 32) {
      jump.play();
    } else {
      explode.play();
    }
  });
}

(() => {
  return run;
})();