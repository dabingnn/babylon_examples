/// <reference path="../babylon.d.ts"/>

function run() {
  let canvas = document.getElementById('renderCanvas');
  let engine = new BABYLON.Engine(canvas, true);
  function createScene() {
    let scene = new BABYLON.Scene(engine);

    let camera = new BABYLON.FreeCamera('camera1', new BABYLON.Vector3(0, 5, -10), scene);

    camera.setTarget(BABYLON.Vector3.Zero());
    camera.attachControl(canvas, false);
    let light = new BABYLON.HemisphericLight('light1', new BABYLON.Vector3(0, 1, 0), scene);
    light.groundColor = new BABYLON.Color3(1, 0, 0);

    let ground = BABYLON.Mesh.CreateGround('ground1', 20, 20, 2, scene);
    ground = BABYLON.Mesh.CreateGroundFromHeightMap('ground','./assets/textures/worldHeightMap.jpg', 
    200, 200, 250, 0, 10, scene, false, (groundMesh)=> {

    });
    ground.position.y = 0;
    let groundMtl = new BABYLON.StandardMaterial('ground', scene);
    groundMtl.diffuseTexture = new BABYLON.Texture('./assets/textures/earth.jpg', scene);
    ground.material = groundMtl;
    return scene;
  }

  let scene = createScene();
  engine.runRenderLoop(() => {
    scene.render();
  });
  window.addEventListener('resize', () => {
    engine.resize();
  });
}

(() => {
  return run;
})();