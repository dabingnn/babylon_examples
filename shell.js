let exampleIndexKey = 'babylon.exampleIndex';
let randomBackGroundColor = ['red', 'green', 'blue']
let randomBackGroundColorIndex = 0;

function _loadPromise(url) {
  return new Promise((resolve, reject) => {
    let xhr = new window.XMLHttpRequest();
    xhr.open('GET', url, true);
    xhr.onreadystatechange = onreadystatechange;
    xhr.send(null);

    function onreadystatechange(e) {
      if (xhr.readyState !== 4) {
        return;
      }

      // Testing harness file:/// results in 0.
      if ([0, 200, 304].indexOf(xhr.status) === -1) {
        reject(`While loading from url ${url} server responded with a status of ${xhr.status}`);
      } else {
        resolve(e.target.response);
      }
    }
  });
}

function _load(view, url) {
  _loadPromise(url).then(result => {
    if (view.firstElementChild) {
      view.firstElementChild.remove();
    }
    let canvas = document.createElement('canvas');
    canvas.classList.add('fit');
    canvas.tabIndex = -1;
    canvas.style.backgroundColor = randomBackGroundColor[randomBackGroundColorIndex];
    canvas.id = 'renderCanvas';
    randomBackGroundColorIndex = (randomBackGroundColorIndex + 1) % 3;
    view.appendChild(canvas);

    window.canvas = canvas;
    _resize();
    let runable = eval(`${result}\n //sourcecode url is ${url}`);
    runable();
  }).catch(err => {
    console.error(err);
  });
}

function _resize() {
  if (!window.canvas) {
    return;
  }

  let bcr = window.canvas.parentElement.getBoundingClientRect();
  window.canvas.width = bcr.width;
  window.canvas.height = bcr.height;
}

document.addEventListener('readystatechange', () => {
  if (document.readyState !== 'complete') {
    return;
  }
  let view = document.getElementById('view');
  let exampleList = document.getElementById('exampleList');
  let exampleIndex = parseInt(localStorage.getItem(exampleIndexKey));
  if (isNaN(exampleIndex)) {
    exampleIndex = 0;
  }
  exampleList.selectedIndex = exampleIndex;

  _load(view, exampleList.value);
  window.addEventListener('resize', () => {
    _resize();
  })

  exampleList.addEventListener('change', event => {
    localStorage.setItem(exampleIndexKey, event.target.selectedIndex);
    _load(view, exampleList.value);
  });
});